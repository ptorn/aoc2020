import * as fs from 'fs';

class Passport {
  byr: number;
  iyr: number;
  eyr: number;
  hgt: string;
  hcl: string;
  ecl: string;
  pid: string;
  cid?: string;

  setData(properties: { [key: string]: any }[]): void {
    if (properties) {
      properties.map(prop => {        
        for (const key in prop) {
          switch (key) {
            case 'byr':
              this.byr = parseInt(prop[key], 10);
              break;
            case 'iyr':
              this.iyr = parseInt(prop[key], 10);
              break;
            case 'eyr':
              this.eyr = parseInt(prop[key], 10);
              break;
            case 'hgt':
              this.hgt = prop[key];
              break;
            case 'hcl':
              this.hcl = prop[key];
              break;
            case 'ecl':
              this.ecl = prop[key];
              break;
            case 'pid':
              this.pid = prop[key];
              break;
            case 'cid':
              this.cid = prop[key];
              break;
            default:
              break;
          }
        }
      });
    }
  }
}

export class Day4 {
  private _mappedPassports: Passport[] = [];

  answer(): void {
    this._readInputFile();
    this._answerOne();
    this._answerTwo();
  }

  private _readInputFile(): void {
    const inputData: string[] = fs.readFileSync('src/day4/input.txt', 'utf-8').split('\n');
    let passport: Passport = new Passport();    
    for (let i = 0; i < inputData.length; i++) {
      if (inputData[i] === '') {
        this._mappedPassports.push(passport);
        passport = new Passport();
        continue;
      }
      let properties: string[] = inputData[i].split(' ');
      let propertiObjects = properties.map(prop => {
        let keyValue = prop.split(':');
        let obj: {[key: string]: string} = {}
        obj[keyValue[0]] = keyValue[1];
        return obj;
      });
      passport.setData(propertiObjects);
    }    
  }

  private _validateFieldsPassport(passport : Passport): boolean {
    return !!passport.byr &&
      !!passport.ecl &&
      !!passport.eyr &&
      !!passport.hcl &&
      !!passport.hgt &&
      !!passport.iyr &&
      !!passport.pid;
  }

  private _validateFieldsAndDataPassport(passport : Passport): boolean {
    if (!this._validateFieldsPassport(passport)) {
      return false;
    }
    const validByr: boolean = this._validateByr(passport.byr);
    const validIyr: boolean = this._validateIyr(passport.iyr);
    const validEyr: boolean = this._validateEyr(passport.eyr);
    const validHgt: boolean = this._validateHgt(passport.hgt);
    const validHcl: boolean = this._validateHcl(passport.hcl);
    const validEcl: boolean = this._validateEcl(passport.ecl);
    const validPid: boolean = this._validatePid(passport.pid);
    return validByr && validIyr && validEyr && validHgt && validHcl && validEcl && validPid;
  }

  private _validateByr(byr: number): boolean {
    return 1920 <= byr && byr <= 2002
  }

  private _validateIyr(iyr: number): boolean {
    return 2010 <= iyr && iyr <= 2020;
  }

  private _validateEyr(eyr: number): boolean {
    return 2020 <= eyr && eyr <= 2030;
  }

  private _validateHgt(hgt: string): boolean {
    const hgtData: RegExpMatchArray | null = hgt.match(/^([0-9]+)(cm|in)$/);
    if (hgtData) {
      const length: number = parseInt(hgtData[1], 10);
      switch (hgtData[2]) {
        case 'cm':
          return 150 <= length && length <= 193;
        case 'in':
          return 59 <= length && length <= 76;
        default:
          break;
      }
    }
    return false;
  }

  private _validateHcl(hcl: string): boolean {
    return !!hcl.match(/^#[0-9a-f]{6}$/);
  }

  private _validateEcl(ecl: string): boolean {
    const validEcl = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'];
    return validEcl.includes(ecl);
  }

  private _validatePid(pid: string): boolean {
    return !!pid.match(/^[0-9]{9}$/);
  }

  private _answerOne(): void {
    const validPassports: Passport[] = this._mappedPassports.filter(passport => this._validateFieldsPassport(passport));    
    console.log('Day4 answer 1: ', validPassports.length);
  }

  private _answerTwo(): void {
    const validPassports: Passport[] = this._mappedPassports.filter(passport => this._validateFieldsAndDataPassport(passport));    
    console.log('Day4 answer 2: ', validPassports.length);
  }
}
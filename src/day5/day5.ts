import * as fs from 'fs';

export class Day5 {
  private _boardingPasses: string[];
  private _rows: number[];
  private _columns: number[];
  private _seatIds: number[];

  constructor() {
  }

  answer(): void {
    this._readInputFile();
    this._decodeSetIds();
    this._answerOne();
    this._answerTwo();
  }

  private _setRowsAndColumns(): void {
    this._rows = [...Array(128).keys()];
    this._columns = [...Array(8).keys()];
  }

  private _getRow(boardingPass: string, seatRows: number[]): number {
    if (seatRows.length === 1) {
      return seatRows[0];
    }
    const rowCode: string = boardingPass.charAt(0);
    switch (rowCode) {
      case 'F':
        seatRows = seatRows.splice(0, seatRows.length / 2);
        break;
      case 'B':
        seatRows = seatRows.splice(seatRows.length / 2, seatRows.length);
        break;
      default:
        break;
    }
    return this._getRow(boardingPass.substring(1), seatRows);
  }

  private _getColumn(boardingPass: string, seatColumns: number[]): number {
    if (seatColumns.length === 1) {
      return seatColumns[0];
    }
    const rowCode: string = boardingPass.charAt(0);
    switch (rowCode) {
      case 'L':
        seatColumns = seatColumns.splice(0, seatColumns.length / 2);
        break;
      case 'R':
        seatColumns = seatColumns.splice(seatColumns.length / 2, seatColumns.length);
        break;
      default:
        break;
    }
    return this._getColumn(boardingPass.substring(1), seatColumns);
  }

  private _getSeatId(row: number, column: number): number {
    return row * 8 + column;
  }

  private _readInputFile():void {
    this._boardingPasses = fs.readFileSync('src/day5/input.txt', 'utf-8').split('\n');
  }

  private _getHighestSeatId(): number {
    const seatIds: number[] = this._seatIds.sort((a, b) => b - a);
    return seatIds[0];
  }

  private _decodeSetIds(): void {
    const seatIds: number[] = [];
    this._boardingPasses.forEach(boardingPass => {
      this._setRowsAndColumns();
      const row: number = this._getRow(boardingPass, this._rows);
      const column: number = this._getColumn(boardingPass, this._columns);
      seatIds.push(this._getSeatId(row, column));
    });
    this._seatIds = seatIds;
  }

  private _findMissingSeatId(): number {
    let missingId: number = -1;
    const seatIds: number[] = this._seatIds.sort((a, b) => a - b);
    for (let i = 1; i < seatIds.length - 1; i++) {
      if (seatIds[i] + 1 === seatIds[i + 1]) {
        continue;
      }
      missingId = seatIds[i] + 1;
      break
    }
    return missingId;
  }

  private _answerOne(): void {
    console.log('Day5 answer 1: ', this._getHighestSeatId());
  }

  private _answerTwo(): void {
    console.log('Day4 answer 2: ', this._findMissingSeatId());
  }
}
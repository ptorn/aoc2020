import * as fs from 'fs';

class Password {
  firstIndex: number;
  secondIndex: number;
  character: string;
  password: string;
}

export class Day2 {
  private _passwordObjects: Password[] = [];

  answer(): void {
    this._readInputFile();
    this._answerOne();
    this._answerTwo();
  }

  private _readInputFile():void {
    this._passwordObjects = fs.readFileSync('src/day2/passwords.txt', 'utf-8').split('\n').map((input: string) => {        
      const inputParams = input.split(/\s|-|:\s/);
      const passwordObj = new Password();
      passwordObj.firstIndex = parseInt(inputParams[0], 10);
      passwordObj.secondIndex = parseInt(inputParams[1], 10);
      passwordObj.character = inputParams[2];
      passwordObj.password = inputParams[3];
      return passwordObj 
    });
  }

  private _countValidPasswords(validator: Function): number {
    let counter: number = 0;
    for (let i = 0; i < this._passwordObjects.length; i++) {
      if (validator(this._passwordObjects[i])) {
        counter++;
      }      
    }
    return counter;
  }

  private _validatePassword(passwordObj: Password): boolean {
    const occurances: number = passwordObj.password.split(passwordObj.character).length - 1;
    return (passwordObj.firstIndex <= occurances) && (occurances <= passwordObj.secondIndex);
  }

  private _validatePasswordToboggan(passwordObj: Password): boolean {
    const firstChar: string = passwordObj.password[passwordObj.firstIndex - 1];
    const secondChar: string = passwordObj.password[passwordObj.secondIndex - 1];
    return (firstChar === passwordObj.character) && (secondChar !== passwordObj.character) || 
    (firstChar !== passwordObj.character) && (secondChar === passwordObj.character)
  }

  private _answerOne(): void {
    const amountValidPasswords = this._countValidPasswords(this._validatePassword);
    console.log('Day2 answer 1: ', amountValidPasswords);
  }

  private _answerTwo(): void {
    const amountValidPasswords = this._countValidPasswords(this._validatePasswordToboggan);
    console.log('Day2 answer 2: ', amountValidPasswords);
  }
}
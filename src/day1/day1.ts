import * as fs from 'fs';

export class Day1 {
  private expenses: number[] = [];

  answer(): void {
    this._readInputFile();
    this._answerOne();
    this._answerTwo();
  }

  private _readInputFile():void {
    this.expenses = fs.readFileSync('src/day1/report.txt', 'utf-8').split('\n').map((expense: string) => parseInt(expense, 10));
  }

  private _getSum2020EntriesTwo(factors: number): number[][] {
    const sums = [];
    for (let i = 0; i < this.expenses.length; i++) {
      for (let n = i + 1; n < this.expenses.length; n++) {
        if ((this.expenses[i] + this.expenses[n]) === 2020) {
          sums.push([this.expenses[i], this.expenses[n]]);
        }
      }
    }
    return sums;
  }

  private _getSum2020EntriesThree(factors: number): number[][] {
    const sums = [];
    for (let i = 0; i < this.expenses.length; i++) {
      for (let n = i + 1; n < this.expenses.length; n++) {
        for (let t = n + 1; t < this.expenses.length; t++) {
          if ((this.expenses[i] + this.expenses[n] + this.expenses[t]) === 2020) {
            sums.push([this.expenses[i], this.expenses[n], this.expenses[t]]);
          }          
        }
      }
    }
    return sums;
  }

  private _multiplyEntries(sums: number[][]): number[] {
    return sums.map(entry => entry.reduce((acc, cur) => {
      return acc * cur;
    }, 1));
  }

  private _answerOne(): void {
    const sums = this._getSum2020EntriesTwo(2);
    const multiplied = this._multiplyEntries(sums);
    console.log('Day1 answer 1: ', multiplied.join(', '));
  }

  private _answerTwo(): void {
    const sums = this._getSum2020EntriesThree(3);
    const multiplied = this._multiplyEntries(sums);
    console.log('Day1 answer 2: ', multiplied.join(', '));
  }
}
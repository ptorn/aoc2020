import { Day1 } from './day1/day1'
import { Day2 } from './day2/day2';
import { Day3 } from './day3/day3';
import { Day4 } from './day4/day4';
import { Day5 } from './day5/day5';

new Day1().answer();
new Day2().answer();
new Day3().answer();
new Day4().answer();
new Day5().answer();

import * as fs from 'fs';

class Map {
  data: string[][];
  width: number;
  height: number;

  constructor(map: string[][]) {
    this.data = map;
    this.height = this.data.length;
    this.width = this.data[0].length;
  }

  getContentFromPosition(posX: number, posY: number): string {
    return this.data[posY][posX];
  }
}

class MapMovePattern {
  right: number;
  down: number;

  constructor(right: number, down: number) {
    this.right = right;
    this.down = down;
  }
}

export class Day3 {
  private _map: Map;
  private _tree: string = '#';
  private _movePattern: MapMovePattern = new MapMovePattern(3, 1);

  answer(): void {
    this._readInputFile();
    this._answerOne();
    this._answerTwo();
  }

  private _readInputFile(): void {
    const inputData: string[][] = fs.readFileSync('src/day3/input.txt', 'utf-8').split('\n').map((row: string) => {        
      return row.split('');
    });
    this._map = new Map(inputData);
  }

  private _countTreesOnPath(map: Map, movePattern: MapMovePattern): number {
    let trees: number = 0;
    let posX: number = 0;
    for (let posY = 0; posY < map.height; posY += movePattern.down) {
      if (map.getContentFromPosition(posX, posY) === this._tree) {
        trees++;
      }
      posX = (posX + movePattern.right) % map.width;
    }
    return trees;
  }

  private _answerOne(): void {
    const amountTrees: number = this._countTreesOnPath(this._map, this._movePattern);
    console.log('Day3 answer 1: ', amountTrees);
  }

  private _answerTwo(): void {
    const movePatterns: MapMovePattern[] = [
      new MapMovePattern(1, 1),
      new MapMovePattern(3, 1),
      new MapMovePattern(5, 1),
      new MapMovePattern(7, 1),
      new MapMovePattern(1, 2)
    ]
    const nrOfTreesPerMovePattern: number[] = movePatterns.map(pattern => this._countTreesOnPath(this._map, pattern));    
    const productOfTrees: number = nrOfTreesPerMovePattern.reduce((acc, cur) => acc * cur, 1);
    console.log('Day3 answer 2: ', productOfTrees);
  }
}